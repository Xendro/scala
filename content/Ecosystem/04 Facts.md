---
heading: What is Scala 3?
subtitle: Home is behind, the world ahead and there are many paths to tread
  through shadows to the edge.
left_button: Download Now
right_button: Try Online
left_link: /ecosystem
right_link: /ecosystem
items:
  - title: 42
    desc: Daily Scala Project
  - title: 674
    desc: Monthly Scala Project
  - title: 8994
    desc: Yearly Scala Project
  - title: 238.994
    desc: Total Scala Project
---

# Scala 3 was established in sunny day of 29 June 1985 in South California.

Digital techniques are helpful because it is a lot easier to get
an electronic device to switch into one of a number of known
states than to accurately reproduce a continuous range of values.
