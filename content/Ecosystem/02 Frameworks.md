---
frameworks:
  - link: www.github.com
    image: images/image 1.png
    illustration: images/Browser.jpg
    desc: Graphic design is the process of visual communication and
      problem-solving using one or more of typography, photography and
      illustration.
    subtitle: Big & Fast Data
    details: |
      Turpis pellentesque ultricies nullam auctor mattis diam enim nisi.

      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    section1_title: Going against the grain, Scala kills its demo
    section1_desc: Graphic design is the process of visual communication and
    section2_title: Going against the grain, Scala kills its demo
    section2_desc: problem-solving using one or more of typography, photography and
    section3_title: Going against the grain, Scala kills its demo
    section3_desc: Graphic design is the process of visual communication and
  - link: www.github.com
    image: images/image 1.png
    illustration: images/Browser.jpg
    desc: Graphic design is the process of visual communication and
      problem-solving using one or more of typography, photography and
      illustration.
    subtitle: Big & Fast Data
    details: |
      Turpis pellentesque ultricies nullam auctor mattis diam enim nisi.

      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    section1_title: Going against the grain, Scala kills its demo
    section1_desc: Graphic design is the process of visual communication and
    section2_title: Going against the grain, Scala kills its demo
    section2_desc: problem-solving using one or more of typography, photography and
    section3_title: Going against the grain, Scala kills its demo
    section3_desc: Graphic design is the process of visual communication and
---

# Frameworks

Graphic design is the process of visual communication
