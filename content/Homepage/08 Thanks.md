---
heading: Thanks

left: This resource for mastering Scala 3 and networking with other professionals is sponsored by [Ziverge Inc](google.com), providing brilliant technical solutions for innovative companies.

right: The Scala programming language is designed **by Dr. Martin Odersky**, and the Scala compiler is built and maintained by EPFL, the Scala Center, Virtus Labs, and open source contributors.
---
