---
heading: Explore
items:
  - title: Learn Scala 3
    image: images/Group-2290.svg
    desc: Master Scala 3 with free resources.
    link: Learn Free
    url: /learn
  - title: Network
    image: images/Group-2291.svg
    desc: Join the Scala3.com community.
    link: Learn Free
    url: /learn
  - title: Ziverge
    image: images/Group-2292.svg
    desc: Get trained and certified in Scala 3.
    link: Learn Free
    url: /learn
  - title: Scala Lang
    image: images/Group-2289.svg
    desc: Visit the official Scala website.
    link: Learn Free
    url: /learn
---
