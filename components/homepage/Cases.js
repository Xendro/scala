import Content from "../../content/Homepage/05 Use Cases.mdx"
import { Swiper, SwiperSlide } from "swiper/react"

// import Swiper core and required modules
import SwiperCore, { Pagination } from "swiper/core"

// install Swiper modules
SwiperCore.use([Pagination])

const Card = ({ title, desc, image }) => {
  return (
    <div className="card">
      <img src={image} className="card-img-top" />
      <div className="card-body">
        <h4 id="data-engineering">{title}</h4>
        <p>{desc}</p>
      </div>
    </div>
  )
}

const Cases = () => {
  return (
    <div className="cases">
      <div className="container-lg">
        <div className="row">
          <div className="col-lg-4 col-md-8">
            <div className="heading-area">
              <Content />
            </div>
          </div>
        </div>
        <div className="use-case" data-options="use-case">
          <Swiper
            pagination={true}
            slidesPerView={1}
            spaceBetween={30}
            watchOverflow={true}
            breakpoints={{
              576: {
                slidesPerView: 2,
                spaceBetween: 32,
              },
              992: {
                slidesPerView: 3,
                spaceBetween: 46,
              },
            }}>
            <SwiperSlide>
              <Card
                title={"Data Engineering"}
                desc={
                  "Scala is the top choice for data engineering, with proven solutions for analytics, data, streaming, and ML."
                }
                image={"images/case-image-1.png"}
              />
            </SwiperSlide>
            <SwiperSlide>
              <Card
                title={"Microservices"}
                desc={
                  "Scala is used extensively for microservices, including gRPC, HTTP, Thrift, and proprietary protocols.."
                }
                image={"images/case-image-1.png"}
              />
            </SwiperSlide>
            <SwiperSlide>
              <Card
                title={"Web & Mobile Backends"}
                desc={
                  "Scala powers web and mobile backends across REST API and GraphQL protocols, with WebSockets.."
                }
                image={"images/case-image-1.png"}
              />
            </SwiperSlide>
          </Swiper>
        </div>
      </div>
    </div>
  )
}

export default Cases
