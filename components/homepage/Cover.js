import { forwardRef } from "react"
import CoverContent from "../../content/Homepage/01 Cover.mdx"

const Cover = forwardRef((props, ref) => {
  return (
    <div>
      <div className="element-bg top-hero-image">
        <img src="images/master-scala-bg.png" />
      </div>
      <div className="mainscreen top-hero-content">
        <div className=" d-flex justify-content-center align-items-center text-center">
          <div ref={ref} className="mainscreen-cont titleBurrowing">
            <CoverContent />
            <div className="button-group justify-content-center">
              <a href="/download.html" className="button-2">
                Download Sample
              </a>
              <a className="button-2 no-outline">
                {" "}
                Try online
                <img src="images/Icon.png" />
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
})

export default Cover
