/** @jsxRuntime classic */
/** @jsx jsx */
import { css, jsx } from "@emotion/react"
import mq from "../../lib/breakpoints"
import Divider from "../Divider"
import content from "../../content/Homepage/08 Thanks.md"
import matter from "gray-matter"
import { Remarkable } from "remarkable"
const md = new Remarkable()

const data = matter(content).data

const Thanks = () => {
  return (
    <div className="thanks">
      <div className="container-lg">
        <div className="parallax-element thanks-logo">
          <img src="images/thanks-logo.png" />
        </div>
      </div>
      <div className="container-lg">
        <div className="row">
          <div className="col">
            <h2>{data.heading}</h2>
          </div>
        </div>
        <div className="row">
          <div className="col-md-5">
            <div className="thanks-cont">
              <div
                dangerouslySetInnerHTML={{
                  __html: md.render(data.left),
                }}></div>
            </div>
          </div>
          <div className="col-md-5">
            <div
              dangerouslySetInnerHTML={{ __html: md.render(data.right) }}></div>
          </div>
        </div>
      </div>
      <div css={css(mq({ marginTop: 70 }))}>
        <Divider />
      </div>
    </div>
  )
}

export default Thanks
