/** @jsxRuntime classic */
/** @jsx jsx */
import { css, jsx } from "@emotion/react"
import mq from "../lib/breakpoints"
import Content from "../content/Homepage/04 Powered by Scala.mdx"

const logos = [
  "images/powered-by-logos/logo_1.svg",
  "images/powered-by-logos/logo_2.svg",
  "images/powered-by-logos/logo_3.svg",
  "images/powered-by-logos/logo_4.svg",
  "images/powered-by-logos/logo_5.svg",
  "images/powered-by-logos/logo_6.svg",
  "images/powered-by-logos/logo_7.svg",
  "images/powered-by-logos/logo_8.svg",
  "images/powered-by-logos/logo_9.svg",
  "images/powered-by-logos/logo_10.svg",
]

const PoweredByScala = (props) => {
  const items = logos.map((el, i) => {
    return (
      <div
        key={i}
        css={css(
          mq({
            flexBasis: ["50%", "20%", "20%"],
            textAlign: "center",
            padding: "15px 0",
          })
        )}>
        <div
          css={css(
            mq({
              width: [90, 80, 110],
              height: [40, 35, 40],
              backgroundImage: `url(${el})`,
              backgroundRepeat: "no-repeat",
              backgroundSize: "contain",
              backgroundPosition: "center center",
              display: "inline-block",
            })
          )}></div>
      </div>
    )
  })
  return (
    <div className="powered">
      <div className="d-flex justify-content-center text-center">
        <div className="powered-cont">{props.children}</div>
      </div>
      <div
        css={css(
          mq({
            display: "flex",
            flexWrap: "wrap",
            marginTop: [40, 60, 70],
            marginBottom: [50, 70, 100],
          })
        )}
        className="container-lg powered-logos">
        {items}
      </div>
    </div>
  )
}

export default PoweredByScala
