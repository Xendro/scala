import content from "../../content/About/03 Small.md"
import matter from "gray-matter"
import { Remarkable } from "remarkable"
const md = new Remarkable()
import { Swiper, SwiperSlide } from "swiper/react"
import SwiperCore, { Pagination } from "swiper/core"
import { useState } from "react"
SwiperCore.use([Pagination])

const data = matter(content).data

const slides = data.items.map((el, i) => (
  <SwiperSlide key={i}>
    <div style={{ marginBottom: 40 }} className="intro">
      <div className="content-area">
        <h5>{el.title}</h5>
        <p>{el.desc}</p>
      </div>
      <a href={el.link} className="button-3">
        {el.button}
      </a>
      <hr />
    </div>
  </SwiperSlide>
))

const Small = () => {
  return (
    <div className="container">
      <div className="about-us">
        <div className="parallax-element about-header-parallax left top">
          <img src="images/About-Header-Paralax-2.png" />
        </div>
        <div
          className="swiper-container community-card-slider"
          data-options="community-card-slider">
          <Swiper
            breakpoints={{
              768: {
                slidesPerView: 3,
                // spaceBetween: 48,
              },
            }}
            watchOverflow
            pagination={true}
            slidesPerView={1}
            spaceBetween={0}>
            {slides}
          </Swiper>
        </div>
        <div className="d-flex swiper-controls-group">
          <div className="swiper-pagination" />
        </div>
      </div>
    </div>
  )
}

export default Small
