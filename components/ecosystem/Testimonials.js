import content from "../../content/Ecosystem/07 Testimonials.md"
import matter from "gray-matter"
import { Remarkable } from "remarkable"
const md = new Remarkable()
import { Swiper, SwiperSlide } from "swiper/react"
import { useState } from "react"

const data = matter(content).data
const html = {
  __html: md.render(matter(content).content),
}

let swiperInstance

const items = data.items.map((el, i) => {
  return (
    <SwiperSlide key={i}>
      <div className="d-flex">
        <div className="test-left">
          <img src={el.image} />
        </div>
        <div className="test-right">
          <p className="client-words">{el.text}</p>
          <p className="client-name">{el.name}</p>
          <p className="author">{el.desc}</p>
        </div>
      </div>
    </SwiperSlide>
  )
})

const Testimonials = () => {
  const [state, setState] = useState("isBeginning")
  return (
    <div className="container">
      <div className="testimonials">
        <div
          className="parallax-element top right testimonial-ring parallax-animate"
          data-class=".testimonial-ring"
          data-start={5}
          data-end={0}
          data-scroll=".testimonials">
          <img src="images/ring-bg.png" />
        </div>
        <div
          className="testimonial-tag parallax-animate"
          data-class=".testimonial-tag"
          data-start={0}
          data-end={-50}
          data-scroll=".testimonial-tag">
          <p>{data.quote}</p>
        </div>
        <div
          className="testimonial-cont parallax-animate"
          data-class=".testimonial-cont"
          data-start={0}
          data-end={-15}
          data-scroll=".testimonial-cont">
          <div
            className="testimonial-heading"
            dangerouslySetInnerHTML={html}></div>
          {/* Swiper */}
          <div className="testimonial-slider-area">
            <div
              className="swiper-container ourClientsTestimonial"
              data-options="ourClientsTestimonial">
              <Swiper
                onSwiper={(swiper) => (swiperInstance = swiper)}
                onSlideChange={() => {
                  if (swiperInstance.isBeginning) {
                    setState("isBeginning")
                    return
                  }
                  if (swiperInstance.isEnd) {
                    setState("isEnd")
                    return
                  }
                  setState("justSlidin")
                }}
                slidesPerView={1}
                spaceBetween={20}
                watchOverflow={true}>
                {items}
              </Swiper>
            </div>
            <div
              onClick={() => swiperInstance.slidePrev()}
              className={`${
                state != "isBeginning" ? "active" : null
              } testimonial-btn slider-btn swiper-button-prev`}>
              <svg
                width={8}
                height={14}
                viewBox="0 0 8 14"
                fill="none"
                xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M0.287829 6.30465L6.3223 0.287096C6.70617 -0.095891 7.32854 -0.095891 7.71222 0.287096C8.09593 0.669742 8.09593 1.29036 7.71222 1.67297L2.37264 6.99759L7.71206 12.322C8.09577 12.7048 8.09577 13.3254 7.71206 13.708C7.32835 14.0908 6.70601 14.0908 6.32215 13.708L0.287674 7.69037C0.095819 7.49895 0 7.24835 0 6.99762C0 6.74676 0.096005 6.49597 0.287829 6.30465Z"
                  fill="#9DAFBD"
                />
              </svg>
            </div>
            <div
              onClick={() => swiperInstance.slideNext()}
              className={`${
                state != "isEnd" ? "active" : null
              } testimonial-btn slider-btn swiper-button-next`}>
              <svg
                width={8}
                height={14}
                viewBox="0 0 8 14"
                fill="none"
                xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M7.71217 7.69047L1.6777 13.708C1.29383 14.091 0.671461 14.091 0.287783 13.708C-0.0959275 13.3254 -0.0959275 12.7048 0.287783 12.3221L5.62736 6.99753L0.287938 1.6731C-0.0957722 1.2903 -0.0957722 0.669747 0.287938 0.287101C0.671648 -0.0957002 1.29399 -0.0957002 1.67785 0.287101L7.71233 6.30475C7.90418 6.49616 8 6.74677 8 6.9975C8 7.24835 7.904 7.49915 7.71217 7.69047Z"
                  fill="white"
                />
              </svg>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Testimonials
