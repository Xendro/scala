const Divider = ({ left, center, right }) => {
  let type = "left"
  if (center) type = "center"
  if (right) type = "right"
  return (
    <div className={`dividerLine dividerLine--${type} container-lg`}>
      <div className="row">
        <div className="col">
          <div className="dividerLine-underline" />
        </div>
      </div>
      <div className="row">
        <div className="col">
          <div className="dividerLine-highlight dividerLine-highlight--left" />
        </div>
        <div className="col">
          <div className="dividerLine-highlight dividerLine-highlight--center" />
        </div>
        <div className="col">
          <div className="dividerLine-highlight dividerLine-highlight--right" />
        </div>
      </div>
    </div>
  )
}

export default Divider
