import content from "../../content/Community/02 Cards.md"
import matter from "gray-matter"
import { Remarkable } from "remarkable"
const md = new Remarkable()
import { Swiper, SwiperSlide } from "swiper/react"
import SwiperCore, { Pagination } from "swiper/core"
import { useState } from "react"
SwiperCore.use([Pagination])

const data = matter(content).data
const html = {
  __html: md.render(matter(content).content),
}

const slides = data.cards.map((el, i) => (
  <SwiperSlide key={i}>
    <div className="swiper-slide">
      <div style={{ marginBottom: 70 }} className="community-card">
        <img src={el.image} className="card-img" />
        <h4 className="card-heading">{el.heading}</h4>
        <div className="d-flex descrip">
          <img src={el.icon} />
          <p>{el.desc}</p>
        </div>
      </div>
    </div>
  </SwiperSlide>
))
const Cards = () => {
  return (
    <div className="cards">
      <div
        className="swiper-container community-card-slider"
        data-options="community-card-slider">
        <Swiper
          pagination={true}
          slidesPerView={1}
          spaceBetween={30}
          watchOverflow={true}
          breakpoints={{
            768: {
              slidesPerView: 3,
              spaceBetween: 18,
            },
            992: {
              slidesPerView: 3,
              spaceBetween: 46,
            },
          }}>
          {slides}
        </Swiper>
      </div>
    </div>
  )
}

export default Cards
