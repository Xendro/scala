/** @jsxRuntime classic */
/** @jsx jsx */
import { css, jsx } from "@emotion/react"
import mq from "../lib/breakpoints"
import Head from "next/head"
import Header from "../components/Header"
import Divider from "../components/Divider"
import Footer from "../components/Footer"
import ScrollToTop from "../components/ScrollToTop"
import content from "../content/Learn/01 Cover.md"
import bookContent from "../content/Learn/02 Book.md"
import matter from "gray-matter"
import { Remarkable } from "remarkable"
import { createRef, useEffect, useState } from "react"
const md = new Remarkable()
import { Swiper, SwiperSlide } from "swiper/react"
import Slider, { Range } from "rc-slider"
import "rc-slider/assets/index.css"

const data = matter(content).data
const html = {
  __html: md.render(matter(content).content),
}

const bookData = matter(bookContent).data
let swiperInstance

const sliderStyles = css({
  ".rc-slider-rail": {
    backgroundColor: "#555555",
    height: 1,
    top: 6,
  },
  ".rc-slider-handle": {
    backgroundColor: "#FF5C3A",
    border: "none",
  },
  ".rc-slider-track": {
    backgroundColor: "#FF5C3A",
  },
})

const slides = bookData.pages.map((el, i) => (
  <SwiperSlide key={i}>
    <div className="row">
      <div className="col-sm-12">
        <h2>{el.title}</h2>
      </div>
      <div
        className="col-md-6"
        dangerouslySetInnerHTML={{
          __html: md.render(el.left),
        }}></div>
      <div
        className="col-md-6 responsive-m"
        dangerouslySetInnerHTML={{
          __html: md.render(el.right),
        }}></div>
    </div>
  </SwiperSlide>
))

export default function Learn() {
  const [tocState, setTocState] = useState(false)
  const [fullscreen, setFullscreen] = useState(false)
  const [dark, setDark] = useState(false)
  const [slide, setSlide] = useState(0)
  useEffect(() => {
    swiperInstance.slideTo(slide, 0)
  })

  const items = bookData.contents.map((el, i) => (
    <li
      onClick={_ => {
        setSlide(el.page - 1)
        setTocState(false)
      }}
      key={i}>
      <a className="slides-chapter">{el.title}</a>
    </li>
  ))

  const TableOfContent = () => <ul>{items}</ul>

  const elem = createRef()
  useEffect(() => {
    function exitHandler() {
      if (document.webkitIsFullScreen === false) {
        setFullscreen(false)
      } else if (document.mozFullScreen === false) {
        setFullscreen(false)
      } else if (document.msFullscreenElement === false) {
        setFullscreen(false)
      }
    }
    if (document.addEventListener) {
      document.addEventListener("fullscreenchange", exitHandler, false)
      document.addEventListener("mozfullscreenchange", exitHandler, false)
      document.addEventListener("MSFullscreenChange", exitHandler, false)
      document.addEventListener("webkitfullscreenchange", exitHandler, false)
    }
  }, [])
  return (
    <main>
      <div className="element-bg learn-page-bg">
        <img src="images/Learn-Hero-BG.jpg" />
      </div>

      <section style={{ zIndex: 3 }} className="learn-section">
        <div className="container">
          <div className="mainscreen">
            <div className=" d-flex justify-content-center text-center">
              <div className="mainscreen-cont">
                <div dangerouslySetInnerHTML={html}></div>
                <a target="_blank" href={data.url} className="button-3 active">
                  {data.button}
                </a>
              </div>
            </div>
          </div>
        </div>
        <div className="parallax-element bottom left right learnscala-logo">
          <img src="images/scala-logo.png" />
        </div>
        <div className="parallax-element bottom left right learnwaves">
          <img src="images/Learn-planet.png" />
        </div>
      </section>
      <div style={{ zIndex: 2 }} className="parallax-element left learn-circle">
        <img src="images/Sircle-2-Paralx.png" />
      </div>
      <div style={{ zIndex: 2 }} className="parallax-element learn-circle2">
        <img src="images/Sircles-Paralax.png" />
      </div>
      <div
        style={{ zIndex: 3, position: "relative" }}
        className="learn-animate">
        <div className="sponsore">
          <div
            style={{ maxWidth: 500 }}
            dangerouslySetInnerHTML={{ __html: md.render(data.sponsored) }}
            className="m-auto text-center"></div>
        </div>
        <section
          className={dark ? "dark-mode" : ""}
          ref={elem}
          id="full-screen-area">
          <section
            className={"book-section " + (fullscreen ? "full-screen " : " ")}>
            <div className="container">
              <div className="row">
                <div className="col-sm-12 book-panel">
                  <div className="d-flex book-header">
                    <div
                      onClick={() => {
                        setTocState(true)
                      }}
                      className="book-menu text-left">
                      <svg
                        width={23}
                        height={21}
                        viewBox="0 0 23 21"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path
                          fillRule="evenodd"
                          clipRule="evenodd"
                          d="M0 1.4375C0 0.643591 0.643591 0 1.4375 0H21.5625C22.3564 0 23 0.643591 23 1.4375C23 2.23141 22.3564 2.875 21.5625 2.875H1.4375C0.643591 2.875 0 2.23141 0 1.4375ZM0 10.0625C0 9.26859 0.643591 8.625 1.4375 8.625H21.5625C22.3564 8.625 23 9.26859 23 10.0625C23 10.8564 22.3564 11.5 21.5625 11.5H1.4375C0.643591 11.5 0 10.8564 0 10.0625ZM1.4375 17.25C0.643591 17.25 0 17.8936 0 18.6875C0 19.4814 0.643591 20.125 1.4375 20.125H21.5625C22.3564 20.125 23 19.4814 23 18.6875C23 17.8936 22.3564 17.25 21.5625 17.25H1.4375Z"
                          fill="#FF5C3A"
                        />
                      </svg>
                    </div>
                    <div className="book-heading text-center">
                      <label>Book Name</label>
                    </div>
                    <div className="book-control text-right">
                      <a
                        onClick={() => {
                          setDark(!dark)
                        }}
                        className={"light-dark-btn " + (dark ? "dark" : "")}>
                        {dark ? "Light" : "Dark"}
                      </a>
                      <svg
                        onClick={() => {
                          setFullscreen(true)
                          if (elem.current.requestFullscreen) {
                            elem.current.requestFullscreen()
                          } else if (elem.current.webkitRequestFullscreen) {
                            elem.current.webkitRequestFullscreen()
                          } else if (elem.current.msRequestFullscreen) {
                            elem.current.msRequestFullscreen()
                          }
                        }}
                        className="full-screen-btn"
                        width={24}
                        height={24}
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path
                          d="M3.42857 15.4286H0V24H8.57143V20.5714H3.42857V15.4286ZM0 8.57143H3.42857V3.42857H8.57143V0H0V8.57143ZM20.5714 20.5714H15.4286V24H24V15.4286H20.5714V20.5714ZM15.4286 0V3.42857H20.5714V8.57143H24V0H15.4286Z"
                          fill="#FF5C3A"
                        />
                      </svg>
                      <svg
                        onClick={() => {
                          if (document.exitFullscreen) {
                            document.exitFullscreen()
                          } else if (document.webkitExitFullscreen) {
                            document.webkitExitFullscreen()
                          } else if (document.mozCancelFullScreen) {
                            document.mozCancelFullScreen()
                          } else if (document.msExitFullscreen) {
                            document.msExitFullscreen()
                          }
                          setFullscreen(false)
                        }}
                        className="small-screen-btn"
                        width={24}
                        height={24}
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path
                          d="M0 18.8571H5.14286V24H8.57143V15.4286H0V18.8571ZM5.14286 5.14286H0V8.57143H8.57143V0H5.14286V5.14286ZM15.4286 24H18.8571V18.8571H24V15.4286H15.4286V24ZM18.8571 5.14286V0H15.4286V8.57143H24V5.14286H18.8571Z"
                          fill="#FF5C3A"
                        />
                      </svg>
                    </div>
                  </div>
                  <div className="book-body">
                    <div
                      className="swiper-container  bookSlider"
                      data-options="bookSlider">
                      <Swiper
                        spaceBetween={30}
                        onSwiper={swiper => (swiperInstance = swiper)}
                        onSlideChange={() => {
                          setSlide(swiperInstance.activeIndex)
                        }}>
                        {slides}
                      </Swiper>
                      <div
                        onClick={() => swiperInstance.slideNext()}
                        className="swiper-button-next"
                      />
                      <div
                        onClick={() => swiperInstance.slidePrev()}
                        className="swiper-button-prev"
                      />
                    </div>
                    <div
                      css={css(
                        mq({
                          position: "relative",
                          // bottom: 17,
                          // left: 7,
                          // right: 7,
                          // height: 30,
                        })
                      )}>
                      <label
                        data-current-chapter
                        className="slider-label"
                        css={css(
                          mq({
                            left: -1,
                            display: "inline-block",
                            top: -27,
                          })
                        )}>
                        {bookData.pages[slide].title}
                      </label>
                      <label
                        data-current-percentage
                        className="slider-label-percent"
                        css={css(
                          mq({
                            right: 0,
                            left: "auto",
                            display: "inline-block",
                            top: -27,
                          })
                        )}>
                        {Math.ceil(((slide + 1) / bookData.pages.length) * 100)}
                        %
                      </label>
                      <div css={sliderStyles}>
                        <Slider
                          onChange={value => {
                            setSlide(
                              value < bookData.pages.length
                                ? value
                                : bookData.pages.length - 1
                            )
                          }}
                          value={slide}
                          max={bookData.pages.length - 1}
                        />
                      </div>
                    </div>
                    <a
                      onClick={() => swiperInstance.slidePrev()}
                      className="arrow arrow-left">
                      <svg
                        width={17}
                        height={65}
                        viewBox="0 0 17 65"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path
                          d="M16 1L2 31.741L16 64"
                          stroke="#AFAFAF"
                          strokeWidth={2}
                        />
                      </svg>
                    </a>
                    <a
                      onClick={() => swiperInstance.slideNext()}
                      className="arrow arrow-right">
                      <svg
                        width={17}
                        height={65}
                        viewBox="0 0 17 65"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path
                          d="M1 1L15 31.741L1 64"
                          stroke="#AFAFAF"
                          strokeWidth={2}
                        />
                      </svg>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </section>
      </div>
      {tocState && (
        <div>
          <div
            onClick={() => {
              setTocState(false)
            }}
            className="popup-overlay"
          />
          <div className="book-content-popup">
            <div className="d-flex popup-head">
              <h4>Table of Content</h4>
              <a
                onClick={() => {
                  setTocState(false)
                }}
                className="close-book-popup text-right">
                <svg
                  width={23}
                  height={23}
                  viewBox="0 0 23 23"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg">
                  <path
                    d="M23 2.31643L20.6836 0L11.5 9.18357L2.31643 0L0 2.31643L9.18357 11.5L0 20.6836L2.31643 23L11.5 13.8164L20.6836 23L23 20.6836L13.8164 11.5L23 2.31643Z"
                    fill="white"
                  />
                </svg>
              </a>
            </div>
            <div className="popup-body">
              <TableOfContent />
            </div>
          </div>
        </div>
      )}

      {/* Footer */}
      <div className="element-bg footer-sky">
        <img src="images/Learn-footer-BG.jpg" />
      </div>
      <Footer />
    </main>
  )
}
