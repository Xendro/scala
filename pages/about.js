import Head from "next/head"
import Header from "../components/Header"
import Divider from "../components/Divider"
import Footer from "../components/Footer"
import ScrollToTop from "../components/ScrollToTop"

import Cover from "../content/About/01 Cover.mdx"
import Large from "../components/about/Large"
import Small from "../components/about/Small"
import CaseStudy from "../components/about/CaseStudy"
import Features from "../components/about/Features"
import Slider from "../components/about/Slider"

export default function About() {
  return (
    <>
      <main>
        <div className="element-bg about-page-bg">
          <img src="images/About-Sky-header.jpg" />
        </div>

        {/* about section */}
        <section className="about-section">
          <div className="element-bg about-section-middle-bg">
            <img src="images/About-Middle-sky.jpg" />
          </div>
          <div className="element-bg about-section-bg">
            <img src="images/About-Header-Paralax-1.png" />
          </div>
          <div className="container">
            <div className="mainscreen">
              <div className=" d-flex justify-content-center text-center">
                <div className="mainscreen-cont">
                  <Cover />
                  <button className="button-3 active">Join Now</button>
                </div>
              </div>
            </div>
            <Large />
          </div>
          <Small />
          <CaseStudy />
          <div className="container">
            <div className="parallax-element polygon top text-center">
              <svg
                width={842}
                height={729}
                viewBox="0 0 842 729"
                fill="none"
                xmlns="http://www.w3.org/2000/svg">
                <path
                  opacity="0.03"
                  d="M421 729L841.888 0H0.111664L421 729Z"
                  fill="#B387D5"
                />
              </svg>
            </div>
            <Features />
            <Slider />
          </div>
        </section>
        {/* Footer */}
        <div className="element-bg footer-sky">
          <img src="images/About-Footer-sky.jpg" />
        </div>
        <Footer />
      </main>
    </>
  )
}
