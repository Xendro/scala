import Head from "next/head"
import Header from "../components/Header"
import Divider from "../components/Divider"
import Footer from "../components/Footer"
import ScrollToTop from "../components/ScrollToTop"

import Cover from "../content/Community/01 Cover.mdx"
import Cards from "../components/community/Cards"
import Testimonials from "../components/community/Testimonials"
import Experts from "../components/community/Experts"

export default function Community() {
  return (
    <main>
      <div className="element-bg community-page-bg">
        <img src="images/Community_Hero_BG.jpg" />
      </div>

      {/* community section */}
      <section className="community-section">
        <div className="content-animate-top">
          <div className="container-lg">
            <div className="mainscreen">
              <div className=" d-flex justify-content-center text-center">
                <div className="mainscreen-cont">
                  <Cover />
                  <button className="button-3 active">Join Now</button>
                </div>
              </div>
            </div>
            <Cards />
          </div>
          <Divider left />
        </div>
        <div className="parallax-element community-first-pimg">
          <img src="images/Community_Line_1 Paralax.png" />
        </div>
        <div className="parallax-element community-two-pimg">
          <img src="images/Community_Line_2 Paralax.png" />
        </div>
        <div className="parallax-element community-four-pimg">
          <img src="images/Community_Line_4.png" />
        </div>
        <Testimonials />
        <Divider center />
        <Experts />
        <div className="parallax-element left bottom bottom-triangle">
          <svg
            width={1224}
            height={1224}
            viewBox="0 0 1224 1224"
            fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <path
              opacity="0.04"
              d="M0 1224L-5.35027e-05 0L1224 1224L0 1224Z"
              fill="#3C4CDC"
            />
          </svg>
        </div>
      </section>
      <Footer hideBanner />
    </main>
  )
}
