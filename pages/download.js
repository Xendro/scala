import Head from "next/head"
import Header from "../components/Header"
import Divider from "../components/Divider"
import Footer from "../components/Footer"
import ScrollToTop from "../components/ScrollToTop"
import Content from "../content/Download/01 Content.mdx"

export default function Download() {
  return (
    <main>
      <div className="element-bg download-page-bg">
        <img src="images/Download_BG.jpg" />
      </div>

      <section className="download-mainscreen">
        <div className="container">
          <div className="mainscreen">
            <div className=" d-flex justify-content-center text-center">
              <div className="mainscreen-cont">
                <Content />
              </div>
            </div>
          </div>
          <div className="download-options d-flex justify-content-between">
            <div className="text-center">
              <img src="images/windows (2) 1.svg" />
              <h2>Windows</h2>
              <hr />
              <p>Version 1.0.1</p>
              <button className="button-3 active">Download</button>
            </div>
            <div className="text-center">
              <img src="images/apple 1.svg" />
              <h2>Mac</h2>
              <hr />
              <p>Version 1.0.1</p>
              <button className="button-3 active">Download</button>
            </div>
            <div className="text-center">
              <img src="images/linux 1.svg" />
              <h2>Linux</h2>
              <hr />
              <p>Version 1.0.1</p>
              <button className="button-3 active">Download</button>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </main>
  )
}
