import Head from "next/head"
import Header from "../components/Header"
import Divider from "../components/Divider"
import Footer from "../components/Footer"
import ScrollToTop from "../components/ScrollToTop"
import JobCard from "../components/JobCard"
import Content from "../content/Jobs/01 Content.mdx"
import jobsContent from "../content/Jobs/02 List of Jobs.md"
import matter from "gray-matter"

const cards = matter(jobsContent).data.items.map((el, i) => (
  <JobCard key={i} data={el}></JobCard>
))

export default function Jobs() {
  return (
    <main>
      <div
        style={{
          width: "100%",
          overflow: "hidden",
          position: "absolute",
          paddingBottom: "100%",
        }}
        className="element-bg jobs-page-bg">
        <img
          style={{
            width: "200%",
            position: "absolute",
            display: "block",
          }}
          src="images/jobs_header-BG.jpg"
        />
      </div>

      {/* jobs section */}
      <section className="jobs-section">
        <div className="container">
          <div className="mainscreen">
            <div className=" d-flex justify-content-center text-center">
              <div className="mainscreen-cont">
                <Content />
              </div>
            </div>
            <hr />
          </div>
          <div className="jobs">
            <p>OPEN POSITIONS</p>
            {cards}
          </div>
        </div>
      </section>
      <div className="parallax-element job-footer-bg">
        <img
          style={{ width: "100%", display: "block" }}
          src="images/Jobs_footer_bg.png"
        />
      </div>
      <Footer />
    </main>
  )
}
