import facepaint from "facepaint"

const breakpoints = [576, 992, 1200, 1400]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

export default mq
