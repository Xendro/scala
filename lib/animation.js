const home = [
  { query: ".mainscreen-cont", delta: 80 },
  { query: ".whyscala .container-lg" },
  { query: ".points" },
  { query: ".cases" },
  { query: ".powered" },
  { query: ".explore-container" },
  { query: ".explore-sky-parallax" },
  { query: ".features-animate", delta: 60 },
  { query: ".thanks" },
  { query: ".banner", delta: 60 },
  { query: ".thanks-logo img", rotation: true, delta: 0 },
]

const about = [
  { query: ".mainscreen-cont", delta: 120 },
  { query: ".about-section .about-element", delta: 40 },
  { query: ".about-heading" },
  { query: ".about-us .community-card-slider" },
  { query: ".case-study-text" },
  { query: ".case-study-image", delta: 120 },
  { query: ".case-study .desc" },
  { query: ".features" },
  { query: ".design-studio .swiper-slide-active .slider-head" },
]

const download = [
  { query: ".mainscreen", delta: 120 },
  { query: ".download-options" },
]

const ecosystem = [
  { query: ".mainscreen", delta: 120 },
  { query: ".framework-left", delta: 60 },
  { query: ".points", delta: 60 },
  { query: ".parallax-animate--play", delta: 40 },
  { query: ".tools-bottom-half" },
  { query: ".about-scala" },
  { query: ".library" },
  { query: ".powered" },
  { query: ".testimonial-tag", delta: 40 },
  { query: ".testimonial-cont" },
]

const community = [
  { query: ".mainscreen", delta: 120 },
  { query: ".cards" },
  { query: ".testimonials", delta: 60 },
  { query: ".experts_animated-area" },
]

const jobs = [{ query: ".mainscreen" }]

const certification = [{ query: ".container" }]

const learn = [
  { query: ".mainscreen-cont", delta: 120 },
  { query: ".learnwaves", delta: 80 },
  { query: ".learn-animate", delta: 160, origin: "top" },
]

export default {
  "/": home,
  "/about": about,
  "/download": download,
  "/ecosystem": ecosystem,
  "/community": community,
  "/jobs": jobs,
  "/certification": certification,
  "/learn": learn,
}
